<?php

class Validator {
    public $errors = array();

    public function isNumber($value){
        if(!is_numeric($value)){
            $this->errors['number'] = 'Not a number';
        }
        return $this;
    }

    public function maxLength($value, $max){
        if(strlen($value) > $max){
            $this->errors['max'] = "Bigger then maximum";
        }
        return $this;
    }

    public function minLength($value, $min){
        if(strlen($value) < $min){
            $this->errors['min'] = "Less then minimum";
        }
        return $this;
    }
    public function alphaNumeric($value){
        if(!ctype_alnum($value)){
            $this->errors['alpha'] = "No correct";
        }
        return $this;
    }

    public function checkEmail($value){
        if(!filter_var($value, FILTER_VALIDATE_EMAIL)){
            $this->errors['email'] = "No correct email";
        }
        return $this;
    }

    public function lengthValidate($value, $min, $max){
        if(strlen($value) > $max || strlen($value) < $min){
            $this->errors['length'] = 'Length is not correct!';
        }
        return $this;
    }

    public function checkRequired($value){
        if(empty($value)){
            $this->errors['required'] = "Required fild";
        }
        return $this;
    }

    public function checkPassword($value, $value_confirm){
        if($value !== $value_confirm){
            $this->errors['password'] = 'Password not checked';
        }
        return $this;
    }
    public function checkArray($value){
        if(!is_array($value)){
            $this->errors['array'] = 'This is not array';
        }
        return $this;
    }
    public function checkUrl($value){
        if(!filter_var($value, FILTER_VALIDATE_URL)){
            $this->errors['url'] = 'This is not url';
        }
        return $this;
    }

}