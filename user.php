<?php
class User {
    public $login;
    public $email;
    public $password;
    public $phone;

    public $db_settings = array(
        'login' => 'root',
        'password' => '108',
        'host' => 'localhost',
        'charset' => 'utf8',
        'db_name' => 'easycode'
    );

    public function __construct($login, $email, $password, $phone) {
        $this->login = $login;
        $this->password = $password;
        $this->email = $email;
        $this->phone = $phone;
    }

    public function connect($db_settings) {
        $host = $db_settings['host'];
        $db_name = $db_settings['db_name'];
        $charset = $db_settings['charset'];

        $db = new PDO(
            "mysql:host=$host;
			dbname=$db_name;
			charset=$charset",
            $db_settings['login'],
            $db_settings['password']);

        return $db;
    }
}

